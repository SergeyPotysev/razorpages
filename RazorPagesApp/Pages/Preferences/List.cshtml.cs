using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Preferences
{
    public class ListModel : PageModel
    {
        public List<Preference> PreferenceList { get; set; }
        private readonly IRepository<Preference> _preferenceRepository;

        public ListModel(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }

        public async Task OnGetAsync()
        {
            var preferences = await _preferenceRepository.GetAllAsync();
            PreferenceList = preferences.ToList();
        }


        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            try
            {
                var preference = await _preferenceRepository.GetByIdAsync(id);
                await _preferenceRepository.DeleteAsync(preference);
                return RedirectToPage();
            }
            catch
            {
                return BadRequest($"Failed to delete preference with id: {id}");
            }
        }
    }
}