using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Preferences
{
    public class CreateModel : PageModel
    {
        private readonly IRepository<Preference> _preferenceRepository;

        [BindProperty]
        public Preference Preference { get; set; }
        public string Message { get; private set; }

        public CreateModel(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
            Preference = new Preference
            {
                Id = Guid.NewGuid()
            };
        }


        public void OnGet()
        {
            Message = "";
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            try
            {
                await _preferenceRepository.AddAsync(Preference);
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to create new preference");
            }

        }
    }
}
