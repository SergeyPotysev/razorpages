using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Preferences
{
    public class EditModel : PageModel
    {
        private readonly IRepository<Preference> _preferenceRepository;

        [BindProperty]
        public Preference Preference { get; set; }
        public string Message { get; private set; }

        public EditModel(IRepository<Preference> preferenceRepository)
        {
            _preferenceRepository = preferenceRepository;
        }


        public async Task<IActionResult> OnGetAsync(string id)
        {
            var guid = Guid.Parse(id);
            Preference = await _preferenceRepository.GetByIdAsync(guid);

            if (Preference == null)
                return NotFound();
            
            Message = "";
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            try
            {
                await _preferenceRepository.UpdateAsync(Preference);
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to update preference with id: {Preference.Id}");
            }
        }
    }
}
