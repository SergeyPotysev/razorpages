using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Customers
{
    public class ListModel : PageModel
    {
        public List<Customer> CustomerList { get; set; }
        private readonly IRepository<Customer> _customerRepository;

        public ListModel(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        
        public async Task OnGetAsync()
        {
            var customers = await _customerRepository.GetAllAsync();
            CustomerList = customers.ToList();
        }


        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            try
            {
                var customer = await _customerRepository.GetByIdAsync(id);
                await _customerRepository.DeleteAsync(customer);
                return RedirectToPage();
            }
            catch
            {
                return BadRequest($"Failed to delete customer with id: {id}");
            }            
        }
    }
}