using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Customers
{
    public class EditModel : PageModel
    {
        public List<Customer> CustomerList { get; set; }
        private readonly IRepository<Customer> _customerRepository;

        [BindProperty]
        public Customer Customer { get; set; }
        public string Message { get; private set; }

        public EditModel(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
        }

        public async Task<IActionResult> OnGetAsync(string id)
        {
            var guid = Guid.Parse(id);
            Customer = await _customerRepository.GetByIdAsync(guid);

            if (Customer == null)
                return NotFound();

            Message = "";
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            try
            {
                await _customerRepository.UpdateAsync(Customer);
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to update сustomer with id: {Customer.Id}");
            }
        }
    }
}
