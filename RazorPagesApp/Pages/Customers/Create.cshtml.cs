using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Customers
{
    public class CreateModel : PageModel
    {
        public List<Customer> CustomerList { get; set; }
        private readonly IRepository<Customer> _customerRepository;

        [BindProperty]
        public Customer Customer { get; set; }
        public string Message { get; private set; }

        public CreateModel(IRepository<Customer> customerRepository)
        {
            _customerRepository = customerRepository;
            Customer = new Customer
            {
                Id = Guid.NewGuid()
            };
        }


        public void OnGet()
        {
            Message = "";
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            try
            {
                await _customerRepository.AddAsync(Customer);
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to create new customer");
            }

        }
    }
}
