using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.PromoCodes
{
    public class CreateModel : PageModel
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Partner> _partnerRepository;

        [BindProperty]
        public PromoCode PromoCode { get; set; }
        public string Message { get; private set; }
        public List<SelectListItem> PreferenceItems { get; set; }
        public List<SelectListItem> RoleItems { get; set; }
        public List<SelectListItem> PartnerItems { get; set; }
        
        [BindProperty]
        public string PreferenceSelected { get; set; }
        [BindProperty]
        public string ManagerSelected { get; set; }
        [BindProperty]
        public string PartnerSelected { get; set; }
        
        public CreateModel(IRepository<PromoCode> promoCodeRepository,IRepository<Preference> preferenceRepository,
            IRepository<Role> roleRepository, IRepository<Partner> partnerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _roleRepository = roleRepository;
            _partnerRepository = partnerRepository;
            PromoCode = new PromoCode
            {
                Id = Guid.NewGuid()
            };
        }


        public async Task OnGet()
        {
            Message = "";
            await FillPartners();
            await FillEmployees();
            await FillPreferences();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            try
            {
                var partners = await _partnerRepository.GetAllAsync(); 
                PromoCode.PartnerId = partners.Single(x => x.Name == PartnerSelected).Id;
                
                var roles = await _roleRepository.GetAllAsync();
                PromoCode.PartnerManagerId = roles.Single(x => x.Name == ManagerSelected).Id;
                
                var preferences = await _preferenceRepository.GetAllAsync();
                PromoCode.PreferenceId = preferences.Single(x => x.Name == PreferenceSelected).Id;
                
                await _promoCodeRepository.AddAsync(PromoCode);
                
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to create new promoCode");
            }
        }

        
        private async Task FillPartners()
        {
            PartnerItems = new List<SelectListItem>();
            var partners = await _partnerRepository.GetAllAsync();
            var partnerNames = partners.Select(x => x.Name).ToList();
            PartnerItems.Add(new SelectListItem
            {
                Value = "0", 
                Text = "Выберите партнера"
            });
            foreach (var partnerName in partnerNames)
            {
                PartnerItems.Add(new SelectListItem
                {
                    Value = partnerName, 
                    Text = partnerName
                });
            }
        }
        
        
        private async Task FillEmployees()
        {
            RoleItems = new List<SelectListItem>();
            var roles = await _roleRepository.GetAllAsync();
            var roleNames = roles.Select(x => x.Name).ToList();
            RoleItems.Add(new SelectListItem
            {
                Value = "0", 
                Text = "Выберите сотрудника"
            });
            foreach (var roleName in roleNames)
            {
                RoleItems.Add(new SelectListItem
                {
                    Value = roleName, 
                    Text = roleName
                });
            }
        }

        
        private async Task FillPreferences()
        {
            PreferenceItems = new List<SelectListItem>();
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferenceNames = preferences.Select(x => x.Name).ToList();
            PreferenceItems.Add(new SelectListItem
            {
                Value = "0", 
                Text = "Выберите предпочтение"
            });
            foreach (var preferenceName in preferenceNames)
            {
                PreferenceItems.Add(new SelectListItem
                {
                    Value = preferenceName, 
                    Text = preferenceName
                });
            }
        }
    }
}
