using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.PromoCodes
{
    public class ListModel : PageModel
    {
        public List<PromoCodeShow> PromoCodeList { get; set; } = new List<PromoCodeShow>();
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Role> _roleRepository;
        private readonly IRepository<Partner> _partnerRepository;

        public ListModel(IRepository<PromoCode> promoCodeRepository, IRepository<Preference> preferenceRepository,
            IRepository<Partner> partnerRepository, IRepository<Role> roleRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _partnerRepository = partnerRepository;
            _roleRepository = roleRepository;
        }


        public async Task OnGetAsync()
        {
            var promoCodes = await _promoCodeRepository.GetAllAsync();
            var preferences = await _preferenceRepository.GetAllAsync();
            var partners = await _partnerRepository.GetAllAsync();
            var roles = await _roleRepository.GetAllAsync();
            
            foreach (var promoCode in promoCodes)
            {
                PromoCodeList.Add(new PromoCodeShow
                {
                    Id = promoCode.Id,
                    Code = promoCode.Code,
                    ServiceInfo = promoCode.ServiceInfo,
                    BeginDate = promoCode.BeginDate,
                    EndDate = promoCode.EndDate,
                    Partner = partners.Single(x => x.Id == promoCode.PartnerId).Name,
                    PartnerManager = roles.Single(x => x.Id == promoCode.PartnerManagerId).Name,
                    Preference = preferences.Single(x => x.Id == promoCode.PreferenceId).Name
                });
            }
        }


        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            try
            {
                var promoCode = await _promoCodeRepository.GetByIdAsync(id);
                await _promoCodeRepository.DeleteAsync(promoCode);
                return RedirectToPage();
            }
            catch
            {
                return BadRequest($"Failed to delete promoCode with id: {id}");
            }            
        }
    }
}