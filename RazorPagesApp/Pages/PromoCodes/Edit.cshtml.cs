using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.PromoCodes
{
    public class EditModel : PageModel
    {
        private readonly IRepository<PromoCode> _promoCodeRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<Partner> _partnerRepository;
        private readonly IRepository<Role> _roleRepository;

        [BindProperty]
        public PromoCode PromoCode { get; set; }
        public string Message { get; private set; }
        public List<SelectListItem> PreferenceItems { get; set; }
        public List<SelectListItem> RoleItems { get; set; }
        public List<SelectListItem> PartnerItems { get; set; }
        
        [BindProperty]
        public string PreferenceSelected { get; set; }
        [BindProperty]
        public string ManagerSelected { get; set; }
        [BindProperty]
        public string PartnerSelected { get; set; }
        

        public EditModel(IRepository<PromoCode> promoCodeRepository,IRepository<Preference> preferenceRepository,
            IRepository<Role> roleRepository, IRepository<Partner> partnerRepository)
        {
            _promoCodeRepository = promoCodeRepository;
            _preferenceRepository = preferenceRepository;
            _roleRepository = roleRepository;
            _partnerRepository = partnerRepository;
        }


        public async Task<IActionResult> OnGetAsync(string id)
        {
            var guid = Guid.Parse(id);
            PromoCode = await _promoCodeRepository.GetByIdAsync(guid);

            if (PromoCode == null)
                return NotFound();

            Message = "";
            await FillPartners();
            await FillEmployees();
            await FillPreferences();
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            try
            {
                var partners = await _partnerRepository.GetAllAsync(); 
                PromoCode.PartnerId = partners.Single(x => x.Name == PartnerSelected).Id;
                
                var roles = await _roleRepository.GetAllAsync();
                PromoCode.PartnerManagerId = roles.Single(x => x.Name == ManagerSelected).Id;
                
                var preferences = await _preferenceRepository.GetAllAsync();
                PromoCode.PreferenceId = preferences.Single(x => x.Name == PreferenceSelected).Id;
                
                await _promoCodeRepository.UpdateAsync(PromoCode);
                
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to update promoCode with id: {PromoCode.Id}");
            }
        }
        
        
        private async Task FillPartners()
        {
            PartnerItems = new List<SelectListItem>();
            var partners = await _partnerRepository.GetAllAsync();
            var partnerNames = partners.Select(x => x.Name).ToList();
            foreach (var partnerName in partnerNames)
            {
                var id = partners.Single(x => x.Name == partnerName).Id;
                PartnerItems.Add(new SelectListItem
                {
                    Value = partnerName, 
                    Text = partnerName,
                    Selected = PromoCode.PartnerId == id
                });
            }
        }
        
        
        private async Task FillEmployees()
        {
            RoleItems = new List<SelectListItem>();
            var roles = await _roleRepository.GetAllAsync();
            var roleNames = roles.Select(x => x.Name).ToList();
            foreach (var roleName in roleNames)
            {
                var id = roles.Single(x => x.Name == roleName).Id;
                RoleItems.Add(new SelectListItem
                {
                    Value = roleName, 
                    Text = roleName,
                    Selected = PromoCode.PartnerManagerId == id
                });
            }
        }

        
        private async Task FillPreferences()
        {
            PreferenceItems = new List<SelectListItem>();
            var preferences = await _preferenceRepository.GetAllAsync();
            var preferenceNames = preferences.Select(x => x.Name).ToList();
            foreach (var preferenceName in preferenceNames)
            {
                var id = preferences.Single(x => x.Name == preferenceName).Id;
                PreferenceItems.Add(new SelectListItem
                {
                    Value = preferenceName, 
                    Text = preferenceName,
                    Selected = PromoCode.PreferenceId == id
                });
            }
            
        }
    }
}
