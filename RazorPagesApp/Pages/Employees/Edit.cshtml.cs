using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;

namespace RazorPagesApp.Pages.Employees
{
    public class EditModel : PageModel
    {
        private readonly Repositories.IRepository<Employee> _employeeRepository;
        private readonly Repositories.IRepository<Role> _rolesRepository;

        [BindProperty]
        public Employee Employee { get; set; }
        public string Message { get; private set; }

        public EditModel(Repositories.IRepository<Employee> employeeRepository, Repositories.IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }


        public async Task<IActionResult> OnGetAsync(string id)
        {
            var guid = Guid.Parse(id);
            Employee = await _employeeRepository.GetByIdAsync(guid);

            if (Employee == null)
                return NotFound();

            Message = "";
            return Page();
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            // Проверка правильности написания роли
            var roles = await _rolesRepository.GetAllAsync();
            var roleList = roles.Select(x => x.Name).ToList();
            var roleIsValid = roleList.Where(x => x == Employee.Role.Name).Any();
            if (!roleIsValid)
            {
                Message = "Ошибка в написании роли";
                return Page();
            }

            try
            {
                Employee.Role = roles.SingleOrDefault(x => x.Name == Employee.Role.Name);
                await _employeeRepository.UpdateAsync(Employee);
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to update employee with id: {Employee.Id}");
            }
        }
    }
}
