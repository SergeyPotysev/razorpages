using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Employees
{
    public class CreateModel : PageModel
    {
        private readonly IRepository<Employee> _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        [BindProperty]
        public Employee Employee { get; set; }
        public string Message { get; private set; }


        public CreateModel(IRepository<Employee> employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
            Employee = new Employee
            {
                Id = Guid.NewGuid()
            };
        }


        public void OnGet()
        {
            Message = "";
        }


        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                Message = "Введены некорректные данные!";
                return Page();
            }

            // Проверка правильности написания роли
            var roles = await _rolesRepository.GetAllAsync();
            var roleList = roles.Select(x => x.Name).ToList();
            var roleIsValid = roleList.Where(x => x == Employee.Role.Name).Any();
            if (!roleIsValid)
            {
                Message = "Ошибка в написании роли";
                return Page();
            }

            try
            {
                Employee.Role = roles.SingleOrDefault(x => x.Name == Employee.Role.Name);
                await _employeeRepository.AddAsync(Employee);
                return RedirectToPage("List");
            }
            catch
            {
                return BadRequest($"Failed to create new employee");
            }

        }
    }
}
