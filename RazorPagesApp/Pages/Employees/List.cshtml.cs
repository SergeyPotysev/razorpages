using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RazorPagesApp.Models;
using RazorPagesApp.Repositories;

namespace RazorPagesApp.Pages.Employees
{
    public class ListModel : PageModel
    {
        public List<Employee> EmployeeList { get; set; }
        private readonly IRepository<Employee> _employeeRepository;

        public ListModel(IRepository<Employee> employeeRepository)
        {
            _employeeRepository = employeeRepository;
        }

        public async Task OnGetAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();
            EmployeeList = employees.ToList();

        }


        public async Task<IActionResult> OnPostDeleteAsync(Guid id)
        {
            try
            {
                var employee = await _employeeRepository.GetByIdAsync(id);
                await _employeeRepository.DeleteAsync(employee);
                return RedirectToPage();
            }
            catch
            {
                return BadRequest($"Failed to delete employee with id: {id}");
            }            
        }
    }
}