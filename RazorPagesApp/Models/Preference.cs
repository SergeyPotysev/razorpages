﻿namespace RazorPagesApp.Models
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
    }
}