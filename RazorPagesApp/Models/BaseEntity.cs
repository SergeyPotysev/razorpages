﻿using System;

namespace RazorPagesApp.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}