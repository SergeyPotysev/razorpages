﻿using System;

namespace RazorPagesApp.Models
{
    public class PromoCodeShow
        : BaseEntity
    {
        public string Code { get; set; }

        public string ServiceInfo { get; set; }

        public DateTime BeginDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Partner { get; set; }

        public string PartnerManager { get; set; }
        
        public string Preference { get; set; }
    }
}