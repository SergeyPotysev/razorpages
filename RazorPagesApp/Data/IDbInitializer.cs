﻿namespace RazorPagesApp.Data
{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}