﻿using System;
using System.Collections.Generic;
using System.Linq;
using RazorPagesApp.Models;

namespace RazorPagesApp.Data
{
    public static class FakeDataFactory
    {
        public static List<Employee> Employees => new List<Employee>()
        {
            new Employee()
            {
                Id = Guid.Parse("9af53cb1-1bc7-4606-80ee-616815d0f60e"),
                Email = "owner@somemail.ru",
                FirstName = "Иван",
                LastName = "Сергеев",
                Role = Roles.FirstOrDefault(x => x.Name == "Admin"),
                AppliedPromocodesCount = 5
            },
            new Employee()
            {
                Id = Guid.Parse("a96d9145-9e07-43f2-849b-345d9b6c89e8"),
                Email = "andreev@somemail.ru",
                FirstName = "Петр",
                LastName = "Андреев",
                Role = Roles.FirstOrDefault(x => x.Name == "PartnerManager"),
                AppliedPromocodesCount = 10
            },
        };

        public static List<Role> Roles => new List<Role>()
        {
            new Role()
            {
                Id = Guid.Parse("1307e495-c311-4942-9abf-0a343c8b91ef"),
                Name = "Admin",
                Description = "Администратор",
            },
            new Role()
            {
                Id = Guid.Parse("5343d5ab-9a44-42c2-b866-af11204884b2"),
                Name = "PartnerManager",
                Description = "Партнерский менеджер"
            }
        };
        
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static List<Customer> Customers
        {
            get
            {
                var customerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0");
                var customers = new List<Customer>()
                {
                    new Customer()
                    {
                        Id = customerId,
                        Email = "ivan_sergeev@mail.ru",
                        FirstName = "Иван",
                        LastName = "Петров",
                        Preferences = new List<CustomerPreference>()
                        {
                            new CustomerPreference()
                            {
                                CustomerId = customerId,
                                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
                            },
                            new CustomerPreference()
                            {
                                CustomerId = customerId,
                                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
                            }
                        }
                    }
                };

                return customers;
            }
        }
        
        public static List<Partner> Partners => new List<Partner>()
        {
            new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020,07,9),
                        EndDate = new DateTime(2020,10,9),
                        Limit = 100 
                    }
                }
            },
            new Partner()
            {
                Id = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319"),
                Name = "Каждому кота",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("c9bef066-3c5a-4e5d-9cff-bd54479f075e"),
                        CreateDate = new DateTime(2020,05,3),
                        EndDate = new DateTime(2020,10,15),
                        CancelDate = new DateTime(2020,06,16),
                        Limit = 1000 
                    },
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("0e94624b-1ff9-430e-ba8d-ef1e3b77f2d5"),
                        CreateDate = new DateTime(2020,05,3),
                        EndDate = new DateTime(2020,10,15),
                        Limit = 100 
                    },
                }
            },
            new Partner()
            {
                Id = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43"),
                Name = "Рыба твоей мечты",
                IsActive = false,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("0691bb24-5fd9-4a52-a11c-34bb8bc9364e"),
                        CreateDate = new DateTime(2020,07,3),
                        EndDate = DateTime.Now.AddMonths(1),
                        Limit = 100 
                    }
                }
            },
            new Partner()
            {
                Id = Guid.Parse("20d2d612-db93-4ed5-86b1-ff2413bca655"),
                Name = "Промотеатры",
                IsActive = false,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("93f3a79d-e9f9-47e6-98bb-1f618db43230"),
                        CreateDate = new DateTime(2020,09,6),
                        EndDate = DateTime.Now.AddMonths(1),
                        Limit = 15 
                    }
                }
            }
        };
        
        
        public static IEnumerable<PromoCode> PromoCodes => new List<PromoCode>()
        {
            new PromoCode()
            {
                Id = Guid.Parse("C0366AC8-D6E1-472A-B34D-DD1D79195073"),
                Code = "A241",
                ServiceInfo = "Кэшбэк 5% при оплате покупки картой МИР!",
                BeginDate = new DateTime(2021, 1, 1),
                EndDate = new DateTime(2021, 12, 31),
                PartnerId = Guid.Parse("20d2d612-db93-4ed5-86b1-ff2413bca655"),
                PartnerManagerId = Roles.Single(x => x.Name == "PartnerManager").Id,
                PreferenceId = Preferences.Single(x => x.Name == "Театр").Id
            },
            new PromoCode()
            {
                Id = Guid.Parse("AFC0781D-D995-46AE-A1CE-D74BAAAA9633"),
                Code = "Z300",
                ServiceInfo = "Кодовое слово на 300 бонусов!",
                BeginDate = new DateTime(2021, 2, 1),
                EndDate = new DateTime(2021, 11, 15),
                PartnerId = Guid.Parse("0da65561-cf56-4942-bff2-22f50cf70d43"),
                PartnerManagerId = Roles.Single(x => x.Name == "Admin").Id,
                PreferenceId = Preferences.Single(x => x.Name == "Семья").Id              
            },
            new PromoCode()
            {
                Id = Guid.Parse("A53B7349-DAEC-49E0-9B8D-C1DE54F3932F"),
                Code = "F825",
                ServiceInfo = "Скидка 10% на шоу-программы!",
                BeginDate = new DateTime(2021, 3, 1),
                EndDate = new DateTime(2022, 6, 30),
                PartnerId = Guid.Parse("894b6e9b-eb5f-406c-aefa-8ccb35d39319"),
                PartnerManagerId = Roles.Single(x => x.Name == "Admin").Id,
                PreferenceId = Preferences.Single(x => x.Name == "Дети").Id              
            }
        };

    }
}